package nasledovanie2;

class AbstractNasledovanie2 {
    String typeOfObj="Человек";
    String roll;
    String name;

    public AbstractNasledovanie2(String name){
        this.roll=thisroll();
        this.name=name;

        System.out.println("Данный объект создан здесь прежде всего как "+this.typeOfObj);
        System.out.println("Это "+this.name+ " и он " + this.roll);
    }

    String thisroll(){
        this.roll="все и вся";
        return this.roll;
    }

}
